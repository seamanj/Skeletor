

import torch
from torch import nn, Tensor, sigmoid
from torch.autograd import Variable
import numpy as np



class Skeletor_Loss(nn.Module):
    def __init__(self):
        super(Skeletor_Loss, self).__init__()
        self.skeleton_loss_fun = nn.MSELoss()
        self.cls_loss_fun = nn.BCELoss()
        self.alpha = 1
    def forward(self, results_skeleton, targets_skeleton):
        batch_size = results_skeleton.shape[0]
        skeleton_loss = self.skeleton_loss_fun(results_skeleton.view(batch_size, -1),
                              targets_skeleton.view(batch_size, -1)) * 144


        return skeleton_loss
