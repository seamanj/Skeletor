import os
from os.path import *
import sys
from typing import List, Optional
from logging import Logger
import numpy as np

from src.helpers import make_logger, load_config, get_latest_checkpoint,\
    load_checkpoint, make_dir, load_zipped_pickle, save_zipped_pickle, load_openpose_tar_xz


def draw_openpose(root_dir:str, openpose_full_path:str):
    from src.draw import draw2DJoints, drawPlain
    body_mapping_openpose_to_czech = [0, 1, 5, 6, 7, 2, 3, 4]

    keypoints = load_zipped_pickle(openpose_full_path)

    num_frames = keypoints["num_frames"]
    keypoints = keypoints["poses"]
    for i in range(num_frames):
        num_people = len(keypoints[i]['people'])
        frame_data = []
        if num_people == 0:
            drawPlain(foldName='openpose', root_dir=root_dir,  name="%08d.png" % i)
            continue
        elif num_people > 1:
            dist_min = float('inf')
            x_shoulder_center_most_right = keypoints[i]["people"][0]["pose_keypoints_2d"][3]
            idx_people = 0
            for j in range(1, num_people):  # tj : choose the most right people
                x_shoulder_center_curr = keypoints[i]["people"][j]["pose_keypoints_2d"][3]
                if x_shoulder_center_curr > x_shoulder_center_most_right:
                    x_shoulder_center_most_right = x_shoulder_center_curr
                    idx_people = j
        else:
            idx_people = 0


        face_keypoints_2d = keypoints[i]["people"][idx_people]["face_keypoints_2d"]  # poses is 0-based
        face_keypoints_2d = np.reshape(face_keypoints_2d, (-1, 3))
        #
        pose_keypoints_2d = keypoints[i]["people"][idx_people]["pose_keypoints_2d"]
        pose_keypoints_2d = np.reshape(pose_keypoints_2d, (-1, 3))
        #
        hand_right_keypoints_2d = keypoints[i]["people"][idx_people]["hand_right_keypoints_2d"]
        hand_right_keypoints_2d = np.reshape(hand_right_keypoints_2d, (-1, 3))
        #
        hand_left_keypoints_2d = keypoints[i]["people"][idx_people]["hand_left_keypoints_2d"]
        hand_left_keypoints_2d = np.reshape(hand_left_keypoints_2d, (-1, 3))
        #
        singe_person_data = np.concatenate(
            (pose_keypoints_2d[body_mapping_openpose_to_czech].flatten(), hand_right_keypoints_2d.flatten(),
             hand_left_keypoints_2d.flatten()), axis=0)


        X = np.array(singe_person_data).reshape((1, -1)) # tj : turn vertical vector to horizontal vector

        Xx = X[0:X.shape[0], 0:(X.shape[1]):3]  # tj : (583, 50), jump by every 3 elements
        Xy = X[0:X.shape[0], 1:(X.shape[1]):3]  # tj : (583, 50)
        Xw = X[0:X.shape[0], 2:(X.shape[1]):3]  # tj : (583, 50)

        draw2DJoints(Xx, Xy, foldName='openpose', root_dir=root_dir,  name="%08d.png" % i)


def save_original_images(root_dir:str, videos_path:str):
    import cv2

    save_folder = os.path.join(root_dir, "images")
    make_dir(save_folder)

    vidcap = cv2.VideoCapture(videos_path)
    num_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))

    for i in range(num_frames):

        vidcap.set(cv2.CAP_PROP_POS_FRAMES, i)
        # = tj : 0-based index of the frame to be decoded/captured next.
        success, image = vidcap.read()
        if success:
            output_filename = os.path.join(save_folder, '{:08d}.png'.format(i))
            print('output_filename : {}'.format(output_filename))
            image_resized = cv2.resize(image, (720, 720))
            cv2.imwrite(output_filename, image_resized)
        else:
            break

