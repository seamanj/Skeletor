# coding: utf-8
"""
This modules holds methods for generating predictions from a model.
"""
import os
from os.path import *
import sys
from typing import List, Optional
from logging import Logger
import numpy as np
from src.loss import Skeletor_Loss

import torch
import torch.nn as nn
from torch.utils.data import Dataset
from src.model import Model
from src.data import make_data_loader
from src.mask import gen_mask, gen_translated_mask, gen_joint_mask_option
from src.batch_with_mask import BatchWithMask, BatchWithMaskConf
from src.helpers import make_logger, load_config, get_latest_checkpoint,\
    load_checkpoint, make_dir, load_zipped_pickle, save_zipped_pickle, load_openpose_tar_xz
from src.data import load_data, load_test_data
from src.model import build_model
import random
import math
from src.helpers import overwrite_file, append_file, embed_text, set_seed,embed_2text

from src.drawing_for_evaluation import draw_openpose, save_original_images


from operator import itemgetter
# pylint: disable=too-many-arguments,too-many-locals,no-member
def  validate_on_data(model: Model, data: Dataset,
                     batch_size: int,

                     use_cuda: bool,
                     window_size: int,
                     mask_percentage: float,
                     max_validation_batches: int = 0,
                     loss_function: torch.nn.Module = None,
                     log_filename=None, # tj : for validation
                     with_confidence: bool = False,
                     logger=None, # tj : for test
                     stack_output=False
                     ) -> (float, np.array):


    valid_loader=make_data_loader(dataset=data,
                                  batch_size=batch_size,
                                  shuffle=False)

    # disable dropout
    model.eval()
    # don't track gradients during validation
    with torch.no_grad():
        total_loss = 0.


        total_num = 0
        stacked_output = []
        for valid_batch in valid_loader:
            if max_validation_batches != 0 and total_num > max_validation_batches:
                break


            batch_size = valid_batch['skeleton'].shape[0] # tj : real loaded batch size. the last batch size could be less than we expect

            mask = gen_translated_mask(batch_size, window_size, mask_percentage, valid_batch['translated'])
            batch_with_mask_conf = BatchWithMaskConf(valid_batch['skeleton'], mask, valid_batch['confidence'], use_cuda=use_cuda)



            # run as during training with teacher forcing
            if loss_function is not None:

                batch_loss, output, masked_src = model.get_loss_for_batch(
                    batch_with_mask_conf, loss_function=loss_function, with_c=with_confidence)

                total_loss += batch_loss


                if log_filename is not None:
                    append_file(log_filename, "batch %d batch loss: %f\n"%(total_num, batch_loss))

                if logger is not None:
                    logger.info("batch %d batch loss: %f\n"%(total_num, batch_loss))

                total_num += 1

                if stack_output:
                    output = output.reshape(output.size()[0], output.size()[1], -1, 3)
                    stacked_output.append([valid_batch['skeleton'].detach().cpu().numpy(), \
                                           valid_batch['indices'].detach().cpu().numpy(), \
                                           valid_batch['video_name'], \
                                           valid_batch['openpose_name'], \
                                           mask.detach().cpu().numpy(), \
                                           output.detach().cpu().numpy(), \
                                           batch_loss.detach().cpu().numpy(),\
                                           masked_src.detach().cpu().numpy()])
                # = tj : one batch has 32 frames, 32 indices, one video name and one openpose name


        if loss_function is not None:
            # total validation loss
            valid_loss = total_loss / total_num

        else:
            valid_loss = -1


    return valid_loss, stacked_output


def evaluate(cfg_file,
         ckpt:str,
         seq_name:str,
         sub_dir:str,
         output_dir:str,
         logger:Logger=None) -> None:
    #abs = abspath(cfg_file) #tj : remember to fill the working directory in the configuration setting
    cfg = load_config(cfg_file)
    if ckpt is None:
        model_dir = cfg["training"]["model_dir"]
        ckpt = get_latest_checkpoint(model_dir)
        logger.info("the lastest checkpoint is: %s"%ckpt)
        if ckpt is None:
            raise FileNotFoundError("No checkpoint found in directory {}."
                                    .format(model_dir))
        try:
            step = ckpt.split(model_dir+"/")[1].split(".ckpt")[0]
        except IndexError:
            step = "best"

    with_confidence = cfg["training"]["with_confidence"]
    drawing = cfg["evaluating"]["drawing"]
    delete_intermediate = cfg['evaluating']['delete_intermediate']
    use_cuda = cfg["evaluating"].get("use_cuda", False)
    average_radius = cfg["evaluating"].get("average_radius", 0)
    window_size = cfg["data"].get("window_size", 64)
    mid_pos = math.ceil(float(window_size) / 2)
    num_front = mid_pos - 1  # tj : the mid frame itself doesn't count
    num_rear = window_size - mid_pos

    num_front = num_front + average_radius
    num_rear = num_rear + average_radius

    save_folder = os.path.join(output_dir, sub_dir, seq_name)


    mask_percentage = 0

    # load model state from disk
    model_checkpoint = load_checkpoint(ckpt, use_cuda=use_cuda)


    czech_root_dir = "/vol/research/SignPose/tj/dataset/Pheonix_Czech"
    openpose_root_dir = '/vol/research/extol/data/Phoenix2014T/OpenPose720x720px_parsed'
    video_root_dir = '/vol/research/extol/data/Phoenix2014T/Videos720x720px'

    czech_filename = os.path.join(czech_root_dir, sub_dir, "{}_Czech.gklz".format(seq_name))
    openpose_filename = os.path.join(openpose_root_dir, sub_dir, "{}.raw.gklz".format(seq_name))
    video_filename = os.path.join(video_root_dir, sub_dir, "{}.mp4".format(seq_name))

    draw_openpose(root_dir=save_folder, openpose_full_path=openpose_filename)
    save_original_images(root_dir=save_folder, videos_path=video_filename)

    print("czech_filename: %s"%czech_filename)
    print("openpose_filename: %s"%openpose_filename)
    print("video_filename: %s"% video_filename)

    czech_data = load_zipped_pickle(czech_filename)
    czech_skeleton = czech_data['3D_skeleton']
    czech_confidence = czech_data['condifence']
    #czech_skeleton_xyz = np.dstack((czech_skeleton[0],czech_skeleton[1],czech_skeleton[2])) # merge x,y,z dim
    #czech_skeleton_xyz = np.stack((czech_skeleton[0], czech_skeleton[1], czech_skeleton[2]), axis=2)
    num_frame = czech_skeleton.shape[0]


    # tj : padding front
    # https://stackoverflow.com/questions/32171917/copy-2d-array-into-3rd-dimension-n-times-python
    padding_front_skeleton = np.asarray([czech_skeleton[0]]*num_front)
    padding_rear_skeleton = np.asarray([czech_skeleton[-1]]*num_rear)

    padding_front_confidence = np.asarray([czech_confidence[0]]*num_front)
    padding_rear_confidence = np.asarray([czech_confidence[-1]] * num_rear)

    czech_skeleton_padded = np.concatenate((padding_front_skeleton, czech_skeleton, padding_rear_skeleton), axis=0)
    # = tj : note the difference between stack and concatenate,
    # stack : Join a sequence of arrays along a new axis.
    # concatnate : Join a sequence of arrays along an existing axis.

    czech_confidence_padded = np.concatenate((padding_front_confidence, czech_confidence, padding_rear_confidence), axis=0)

    num_frame_padded = czech_skeleton_padded.shape[0]
    skeleton_dim = np.prod(czech_skeleton_padded.shape[1:])
    czech_skeleton_padded = torch.from_numpy(czech_skeleton_padded)
    czech_confidence_padded = torch.from_numpy(czech_confidence_padded)
    # if use_cuda:
    #     czcech_skeleton_padded = czech_skeleton_padded.cuda()

    print('1')

    # build model and load parameters into it
    model = build_model(cfg["model"], skeleton_dim)
    model.load_state_dict(model_checkpoint["model_state"])

    if use_cuda:
        model.cuda()

    buffer = np.zeros((2*average_radius, window_size, skeleton_dim), dtype=np.float32)
    result = np.zeros((num_frame, skeleton_dim), dtype=np.float32)

    for i in range(num_frame_padded - window_size + 1):
        batch_skeleton = czech_skeleton_padded[i:i+window_size, :]
        batch_skeleton = batch_skeleton[None, :, :]

        batch_confidence = czech_confidence_padded[i:i+window_size, :]
        batch_confidence = batch_confidence[None, :, :]

        if use_cuda:
            batch_skeleton = batch_skeleton.cuda()
            batch_confidence = batch_confidence.cuda()

        batch_size = batch_skeleton.shape[0]


        mask = gen_mask_option(batch_size, window_size, mask_percentage, batch_confidence)
        batch_with_mask_conf = BatchWithMaskConf(batch_skeleton, mask, batch_confidence, use_cuda=use_cuda)

        output, _ = model.forward(batch_with_mask_conf.skeleton, batch_with_mask_conf.mask, batch_with_mask_conf.conf, False)
        # = tj : note, the dimension of input : [B, T, 50, 3], B - batch size, T - window size
        # the dimension of output : [B, T, 150]

        output = output.squeeze(0)

        output = output.detach().cpu().numpy()
        # = tj : solve the average
        if i >= 2*average_radius:
            tmp_result = output[mid_pos - 1 - average_radius, :] # tj : here mid_pos - 1, because mid_pos is 1-based.
            for j in range(2*average_radius - 1):
                tmp_result += buffer[j, mid_pos - 1 + (j - average_radius), : ]  # tj : here mid_pos - 1, because mid_pos is 1-based.
            result[i - 2*average_radius, :] = tmp_result / (2*average_radius + 1)


        buffer[-1,:] = output
        np.roll(buffer, -1, axis=0)

    Yx = result[0:result.shape[0], 0:(result.shape[1]):3]
    Yy = result[0:result.shape[0], 1:(result.shape[1]):3]
    Yz = result[0:result.shape[0], 2:(result.shape[1]):3]

    Xx = czech_skeleton[:,:,0]
    Xy = czech_skeleton[:,:,1]
    Xz = czech_skeleton[:, :, 2]



    if drawing:
        from src.draw import drawJoints
        drawJoints(Yx, Yy, Yz, foldName='predict', type="openpose", root_dir=save_folder)
        drawJoints(Xx, Xy, Xz, foldName='origin', type="openpose", root_dir=save_folder)

        image_video_name = join(save_folder, 'images', 'images.mp4')
        image_video_name_text = join(save_folder, 'images', 'images_text.mp4')
        openpose_video_name = join(save_folder, 'openpose', 'openpose.mp4')
        openpose_video_name_text = join(save_folder, 'openpose', 'openpose_text.mp4')
        # origin_video_name = join(save_folder, 'origin', 'origin.mp4')
        # origin_video_name_text = join(save_folder, 'origin', 'origin_text.mp4')
        predict_video_name = join(save_folder, 'predict', 'predict.mp4')
        predict_video_name_text = join(save_folder, 'predict', 'predict_text.mp4')


        gen_iamges_video_command = 'ffmpeg -y -i ' + save_folder + '/images/%08d.png -c:v libx264 ' + image_video_name
        # = tj : start from zero : -start_number 0
        os.system(gen_iamges_video_command)

        gen_openpose_video_command = 'ffmpeg -y -i ' + save_folder + '/openpose/%08d.png -c:v libx264 ' + openpose_video_name
        os.system(gen_openpose_video_command)


        merged_2_video_name = join(save_folder, 'merged_2.mp4')
        merged_3_video_name = join(save_folder, 'merged_3.mp4')
        # merged_4_video_name = join(save_folder, 'merged_4.mp4')


        embed_2text(openpose_video_name, openpose_video_name_text, 'OpenPose', '   (input)', 44)
        # embed_text(origin_video_name, origin_video_name_text, 'Czech')
        embed_2text(predict_video_name, predict_video_name_text, 'Skeletor', '(output)', 43)

        merge_2_command = 'ffmpeg -y -i ' + openpose_video_name_text + ' -i ' + predict_video_name_text + \
                          ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_2_video_name
        # print(merge_command)
        os.system(merge_2_command)


        merge_3_command = 'ffmpeg -y -i ' + image_video_name + ' -i ' + merged_2_video_name + \
                          ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_3_video_name
        # print(merge_command)
        os.system(merge_3_command)

        merged_fold = make_dir(join(save_folder, "merged"))
        extract_command = 'ffmpeg -i ' + merged_3_video_name + ' -start_number 0 ' + merged_fold + '/%08d.png'
        os.system(extract_command)

        # ffmpeg -i merged_4.mp4 -start_number 0 merged/%08d.png
        if delete_intermediate:
            delete_command = 'rm -rf ' + join(save_folder,'images')
            os.system(delete_command)
            delete_command = 'rm -rf ' + join(save_folder,'openpose')
            os.system(delete_command)
            delete_command = 'rm -rf ' + join(save_folder,'origin')
            os.system(delete_command)
            delete_command = 'rm -rf ' + join(save_folder,'predict')
            os.system(delete_command)
            delete_command = 'rm ' + join(save_folder,'merged_2.mp4')
            os.system(delete_command)
            # delete_command = 'rm ' + join(save_folder,'merged_3.mp4')
            # os.system(delete_command)

    save_data = {}
    save_data['seq_name'] = seq_name
    save_data['skeleton'] = np.dstack((Yx, Yy, Yz))
    make_dir(save_folder)
    save_zipped_pickle(save_data, os.path.join(save_folder, "{}_Skeletor.gklz".format(seq_name)))

    print('1')


def test(cfg_file,
         ckpt:str,
         logger:Logger=None) -> None:


    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    result_dir = cfg["testing"]["result_dir"]

    if logger is None:
        make_dir(result_dir)
        logger = make_logger(abspath(join(result_dir, "test.log")))

    # when checkpoint is not specified, take oldest from model dir
    if ckpt is None:
        model_dir = cfg["training"]["model_dir"]
        ckpt = get_latest_checkpoint(model_dir)
        logger.info("the lastest checkpoint is: %s"%ckpt)
        if ckpt is None:
            raise FileNotFoundError("No checkpoint found in directory {}."
                                    .format(model_dir))
        try:
            step = ckpt.split(model_dir+"/")[1].split(".ckpt")[0]
        except IndexError:
            step = "best"

    batch_size = cfg["training"].get(
        "eval_batch_size", cfg["training"]["batch_size"])
    use_cuda = cfg["training"].get("use_cuda", False)
    window_size = cfg["data"].get("window_size", 64)
    mask_percentage = cfg["training"].get("mask_percentage", 0.15)
    eval_mask_percentage = cfg["training"]["eval_mask_percentage"]

    # load the data
    # _, dev_data, test_data = load_data(data_cfg=cfg["data"])

    test_data = load_test_data(data_cfg=cfg["data"])
    # logger.info("train frames:{}".format(train_data.__len__()))
    #logger.info("dev frames:{}".format(dev_data.__len__()))
    logger.info("test frames:{}".format(test_data.__len__()))


    data_to_predict = {"test": test_data}
    #"train": train_data,  "dev": dev_data, "test": test_data

    # load model state from disk
    model_checkpoint = load_checkpoint(ckpt, use_cuda=use_cuda)

    # build model and load parameters into it
    model = build_model(cfg["model"], test_data.skeleton_dim)
    model.load_state_dict(model_checkpoint["model_state"])

    if use_cuda:
        model.cuda()


    num_export = cfg["testing"]["num_export"]
    drawing = cfg["testing"]["drawing"]
    draw_openpose = cfg['testing']['draw_openpose']
    save_original_images = cfg["testing"]["save_original_images"]
    videos_path = abspath(cfg['data']['videos_path'])
    openpose_path = abspath(cfg['data']['openpose_path'])
    headless = cfg["testing"]["headless"]
    select_criterion = cfg["testing"]["select_criterion"]
    with_confidence = cfg['training']['with_confidence']
    for data_set_name, data_set in data_to_predict.items():
        loss, stacked_output = validate_on_data(
           model, data=data_set, batch_size=batch_size,
           use_cuda=use_cuda, loss_function=Skeletor_Loss(),
           window_size = window_size, mask_percentage=eval_mask_percentage, with_confidence=with_confidence, logger=logger, stack_output=True)
        logger.info("Average batch loss on \'%4s\' dataset: %16.12f ", data_set_name, loss)
        selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x: x[1][6], reverse=True)]
        logger.info("Max batch loss on \'%4s\' dataset: %16.12f ", data_set_name,
                    stacked_output[selected_indices[0]][6])
        logger.info("Min batch loss on \'%4s\' dataset: %16.12f ", data_set_name,
                    stacked_output[selected_indices[-1]][6])

        num_total = len(stacked_output)
        assert num_export <= num_total, "num_export should be less than num_total"

        if select_criterion.lower() == "random":
            selected_indices = random.sample(range(num_total), num_export)
        elif select_criterion.lower() == "decrease":
            # tmp = sorted(stacked_output, key=lambda x:x[3], reverse=True)
            # https://stackoverflow.com/questions/6422700/how-to-get-indices-of-a-sorted-array-in-python
            # tj = : [1] get the stacked_output data itself, [3] get the batch loss for one batch data, [0] get the indices
            selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x:x[1][6], reverse=True)]
            selected_indices = selected_indices[:num_export]
        elif select_criterion.lower() == 'increase':
            selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x: x[1][6])]
            selected_indices = selected_indices[:num_export]


        logger.info("select_criterion: %s", select_criterion.lower())

        # stacked_output.append([valid_batch['skeleton'].detach().cpu().numpy(), \
        #                        valid_batch['indices'].detach().cpu().numpy(), \
        #                        valid_batch['video_name'], \
        #                        valid_batch['openpose_name'], \
        #                        mask.detach().cpu().numpy(), \
        #                        output.detach().cpu().numpy(), \
        #                        batch_loss.detach().cpu().numpy(), \
        #                        masked_src.detach().cpu().numpy()])

        selected_indices = [ 80]#452,  340,

        for index in selected_indices:
            logger.info("selected index: %d", index)
            origin = stacked_output[index][0][0]  # tj : the first 0 means skeleton field, the second 0 means the first frame in one batch
            origin = origin.reshape(origin.shape[0], -1)

            indices = stacked_output[index][1][0]  # tj : 1 means indices field, 0 means the first data in the batch
            video_name = stacked_output[index][2][0] # tj : one batch has one openpose name, not one frame has one openpose name

            openpose_name = stacked_output[index][3][0] # tj : one batch has one openpose name, not one frame has one openpose name

            mask = stacked_output[index][4][0] # tj : first frame's mask in the batch, no need further indexing, only one batch's mask passed
            predict = stacked_output[index][5][0]
            predict = predict.reshape(predict.shape[0], -1)

            masked_src = stacked_output[index][7][0]
            masked_src = masked_src.reshape(masked_src.shape[0], -1)

            output_dir = abspath(join(result_dir, data_set_name, str(index)))
            if not isdir(output_dir):
                os.makedirs(output_dir)
            # tj = : force to overwrite
            np.savetxt(join(output_dir, "origin.txt"), origin)
            np.savetxt(join(output_dir, "indices.txt"), indices, fmt="%10i")
            with open(join(output_dir, 'video_names.txt'), 'w') as f:
                #for video_name in video_names: # tj : only one name for one batch, no need iteration
                f.write('%s\n' % video_name)
            with open(join(output_dir, 'openpose_names.txt'), 'w') as f:
                # for openpose_name in openpose_names:
                f.write('%s\n' % openpose_name)
            np.savetxt(join(output_dir, "mask.txt"), mask, fmt="%5i")
            np.savetxt(join(output_dir, "predict.txt"), predict)
            np.savetxt(join(output_dir, "masked_src.txt"), masked_src)

            if save_original_images:
                _save_original_images(output_dir, videos_path)
            if draw_openpose:
                _draw_openpose(output_dir, openpose_path)
            if drawing:
                from src.draw import draw
                draw(output_dir, headless)


def _draw_openpose(root_dir:str, openpose_path:str):
    from src.draw import draw2DJoints, drawPlain
    body_mapping_openpose_to_czech = [0, 1, 5, 6, 7, 2, 3, 4]

    indices = np.loadtxt(join(root_dir, "indices.txt"))
    with open(join(root_dir,'openpose_names.txt'), 'r') as f:
        # https://stackoverflow.com/questions/14676265/how-to-read-a-text-file-into-a-list-or-an-array-with-python
        openpose_names = f.read().splitlines()

    assert len(openpose_names) == 1


    save_folder = os.path.join(root_dir, "openpose")
    make_dir(save_folder)
    prev_openpose_filename = ''
    keypoints = []

    for i in range(len(indices)):
        index = int(indices[i])
        openpose_filename = os.path.join(openpose_path, openpose_names[0])
        if openpose_filename != prev_openpose_filename:
            suffix = openpose_filename.split(".")[-1]
            if suffix == 'gklz':
                keypoints = load_zipped_pickle(openpose_filename)
            elif suffix == 'tar':
                keypoints = load_openpose_tar_xz(openpose_filename)
            prev_openpose_filename = openpose_filename

        num_people = len(keypoints[index]['people'])
        frame_data = []
        if num_people == 0:
            drawPlain(foldName='openpose', root_dir=root_dir,  name="%08d.png" % index)
            continue
        elif num_people > 1:
            dist_min = float('inf')
            x_shoulder_center_most_right = keypoints[index]["people"][0]["pose_keypoints_2d"][3]
            idx_people = 0
            for j in range(1, num_people):  # tj : choose the most right people
                x_shoulder_center_curr = keypoints[index]["people"][j]["pose_keypoints_2d"][3]
                if x_shoulder_center_curr > x_shoulder_center_most_right:
                    x_shoulder_center_most_right = x_shoulder_center_curr
                    idx_people = j
        else:
            idx_people = 0


        face_keypoints_2d = keypoints[index]["people"][idx_people]["face_keypoints_2d"]  # poses is 0-based
        face_keypoints_2d = np.reshape(face_keypoints_2d, (-1, 3))
        #
        pose_keypoints_2d = keypoints[index]["people"][idx_people]["pose_keypoints_2d"]
        pose_keypoints_2d = np.reshape(pose_keypoints_2d, (-1, 3))
        #
        hand_right_keypoints_2d = keypoints[index]["people"][idx_people]["hand_right_keypoints_2d"]
        hand_right_keypoints_2d = np.reshape(hand_right_keypoints_2d, (-1, 3))
        #
        hand_left_keypoints_2d = keypoints[index]["people"][idx_people]["hand_left_keypoints_2d"]
        hand_left_keypoints_2d = np.reshape(hand_left_keypoints_2d, (-1, 3))
        #
        singe_person_data = np.concatenate(
            (pose_keypoints_2d[body_mapping_openpose_to_czech].flatten(), hand_right_keypoints_2d.flatten(),
             hand_left_keypoints_2d.flatten()), axis=0)


        X = np.array(singe_person_data).reshape((1, -1)) # tj : turn vertical vector to horizontal vector

        Xx = X[0:X.shape[0], 0:(X.shape[1]):3]  # tj : (583, 50), jump by every 3 elements
        Xy = X[0:X.shape[0], 1:(X.shape[1]):3]  # tj : (583, 50)
        Xw = X[0:X.shape[0], 2:(X.shape[1]):3]  # tj : (583, 50)

        draw2DJoints(Xx, Xy, foldName='openpose', root_dir=root_dir,  name="%08d.png" % index)



def _save_original_images(root_dir:str, videos_path:str):
    import cv2
    indices = np.loadtxt(join(root_dir, "indices.txt"))
    with open(join(root_dir, 'video_names.txt'), 'r') as f:
        # https://stackoverflow.com/questions/14676265/how-to-read-a-text-file-into-a-list-or-an-array-with-python
        video_names = f.read().splitlines()

    #assert len(indices) == len(video_names) # tj : comment out, only one video name for one batch
    assert len(video_names) == 1
    save_folder = os.path.join(root_dir, "images")
    make_dir(save_folder)
    for i in range(len(indices)):
        index = int(indices[i])
        video_filename = os.path.join(videos_path, video_names[0]) # tj : list only has one element, but we need to index it.
        vidcap = cv2.VideoCapture(video_filename)
        print('video_filename : {}'.format(video_filename))

        vidcap.set(cv2.CAP_PROP_POS_FRAMES, index - 1)
        # = tj : 0-based index of the frame to be decoded/captured next.
        success, image = vidcap.read()
        if success:
            output_filename = os.path.join(save_folder, '{:08d}.png'.format(index))
            print('output_filename : {}'.format(output_filename))
            image_resized = cv2.resize(image, (720, 720))
            cv2.imwrite(output_filename, image_resized)
        else:
            break

    print('1')



if __name__ == "__main__":
    _save_original_images('/home/seamanj/Workspace/BERT_skeleton/results/EXP2_0_10/test/80',
                          '/home/seamanj/Workspace/BERT_skeleton/data/videos')