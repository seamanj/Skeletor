import torch
from torch import tensor

class BatchWithMask:
    def __init__(self, torch_batch, mask, use_cuda=False):
        self.skeleton = torch_batch
        self.mask = mask
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.skeleton = self.skeleton.cuda()
        self.mask = self.mask.cuda()



class BatchWithMaskConf:
    def __init__(self, torch_batch, mask, conf=None, use_cuda=False):
        self.skeleton = torch_batch
        self.mask = mask
        self.conf = conf
        self.use_cuda = use_cuda

        if use_cuda:
            self._make_cuda()

    def _make_cuda(self):
        self.skeleton = self.skeleton.cuda()
        self.mask = self.mask.cuda()
        if self.conf is not None:
            self.conf = self.conf.cuda()

