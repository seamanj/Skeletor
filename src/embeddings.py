import torch
from torch import nn, Tensor
from src.helpers import freeze_params
from torch.nn.functional import relu

class Embeddings(nn.Module):
    """Construct the embeddings from word, position and token_type embeddings.
    """
    def __init__(self,
                 skeleton_dim: int = 150,
                 embedding_dim: int = 64,
                 freeze: bool = False):
        super(Embeddings, self).__init__()

        self.skeleton_dim = skeleton_dim
        self.embedding_dim = embedding_dim
        self.emb = nn.Linear(skeleton_dim, embedding_dim)
        self.layer_norm = nn.LayerNorm(embedding_dim, eps=1e-6)
        self.conf_ln = nn.Linear(50, 1)
        # TODO: use soft coding instead of hard coding
        if freeze:
            freeze_params(self)

    def forward(self, x: Tensor, c: Tensor ) -> (Tensor, Tensor):
        batch_size, window_size = x.shape[0:2]
        x = x.view(batch_size, window_size, -1)
        x = self.emb(x)
        x = relu(x)
        x = self.layer_norm(x)
        c = torch.mean(c, dim=2, keepdim=True)
        # c = self.conf_ln(c)
        # c = relu(c)
        return x, c

    def __repr__(self):
        return "%s(skeleton_dim=%d, embedding_dim=%d)" % (
            self.__class__.__name__, self.skeleton_dim, self.embedding_dim)