import numpy as np
import random
import torch
from torch import Tensor


def shuffle_2D_in_rows(arr):
    x, y = arr.shape
    rows = np.indices((x, y))[1]
    cols = [np.random.permutation(y) for _ in range(x)]
    return arr[:, cols]


#  tj : bit 1 means mask, means
def gen_mask(batch_size: int, window_size: int, mask_percentage: float, conf, with_sequence_ordering = False) -> Tensor:

        assert mask_percentage >= 0. and mask_percentage <= 1., \
            "mask_percentage should be between 0 and 1."

        if conf is not None and mask_percentage != 0:
            num_masked = int(mask_percentage * window_size)
            num_conf_masked = int(mask_percentage * 0.666666667 * window_size)
            num_random_masked = num_masked - num_conf_masked

            conf_ave = torch.mean(conf, dim=2)
            sorted, indices = torch.sort(conf_ave, 1) # tj : dim 0 means along downward axis, dim 1 means along right axis
            conf_mask_indices = indices[:, :num_conf_masked]

            # tj : https://discuss.pytorch.org/t/take-random-sample-of-long-tensor-create-new-subset/36244/3
            conf_unmask_indices = indices[:, num_conf_masked:]
            num_conf_unmasked = conf_unmask_indices.shape[1]
            rand_indices = [torch.randperm(num_conf_unmasked)[:num_random_masked] for _ in range(batch_size)] #  tj : randperm : from 0 to n-1
            # = tj : we permute each row
            # = tj : permute first, then take the first 'num_random_masked' elements
            rand_indices = torch.stack(rand_indices, dim=0)
            # = tj : stack the list into an array
            rand_mask_indices = torch.gather(conf_unmask_indices, 1, rand_indices)
            # = tj : choose the indices randomly from the masks not covered by confident mask
            # + tj : first get list, and then stack.
            # https://stackoverflow.com/questions/57917981/pytorch-how-to-stack-tensor-like-for-loop
            mask_indices = torch.cat((conf_mask_indices, rand_mask_indices), 1)
            mask = torch.zeros((batch_size, window_size)).scatter(1, mask_indices, torch.ones((mask_indices.shape)))
            # print('1')
            mask = ( mask != 0)
            if with_sequence_ordering:
                mask = torch.cat((torch.tensor(False).repeat(mask.shape[0],1), mask), dim=1)  # shape : (64, 33)
            # print('1')
        else:
            mask = np.full((batch_size,window_size), False)
            num_masked = int(mask_percentage * window_size)
            mask[:,:num_masked] = True
            [random.shuffle(mask[i,:]) for i in range(batch_size)]
            if with_sequence_ordering:
                mask = np.concatenate((np.full((batch_size,1),False), mask), axis=1) # = tj : the first is the cls specular token
            mask = torch.from_numpy(mask) # tj : convert to tensor

        return mask


def gen_translated_mask(batch_size: int, window_size: int, mask_percentage: float, translated):
    assert mask_percentage >= 0. and mask_percentage <= 1., \
        "mask_percentage should be between 0 and 1."

    if mask_percentage == 0:
        mask = torch.zeros((batch_size, window_size), dtype=torch.uint8)
    else:
        mask = torch.zeros((batch_size, window_size), dtype=torch.uint8)
        num_masked = int(mask_percentage * window_size)
        translated = translated.detach().cpu().numpy()
        for i in np.arange(batch_size):
            tran = translated[i,:]
            candidate = []
            for j in np.arange(window_size - num_masked + 1):
                if np.sum(translated[i,j:j+num_masked]) == num_masked:
                    candidate.append(j)
            if len(candidate) > 0:
                cand = random.choice(candidate)
                mask[i,cand:cand+num_masked] = 1
        # print('1')
    return mask



def gen_joint_mask_option(batch_size: int, window_size: int, mask_percentage: float, conf, option=None) -> Tensor:
    assert mask_percentage >= 0. and mask_percentage <= 1., \
        "mask_percentage should be between 0 and 1."

    if mask_percentage != 0 and option == 'highest' or option == 'lowest':
        joint_size = conf.shape[2]
        num_joint_masked = int(mask_percentage * window_size * joint_size)

        if option == 'highest':
            sorted, indices = torch.sort(conf.view(batch_size, -1), 1, descending=True)  # tj : dim 0 means along downward axis, dim 1 means along right axis
        elif option == 'lowest':
            sorted, indices = torch.sort(conf.view(batch_size, -1), 1, descending=False)
        mask_indices = indices[:, :num_joint_masked]
        
        mask_frame_indices = mask_indices / joint_size
        mask_joint_indices = mask_indices % joint_size

        mask = torch.zeros((batch_size, window_size * joint_size)).scatter(1, mask_indices, torch.ones((mask_indices.shape)))
        mask = mask.view((-1, window_size, joint_size))
        # print('1')
        mask = (mask != 0)
        # print('1')
    elif option == 'random':
        joint_size = conf.shape[2]
        mask = np.full((batch_size, window_size * joint_size), False)
        num_joint_masked = int(mask_percentage * window_size * joint_size)
        mask[:, :num_joint_masked] = True
        [random.shuffle(mask[i, :]) for i in range(batch_size)]
        mask = mask.reshape((-1, window_size, joint_size))
        mask = torch.from_numpy(mask)  # tj : convert to tensor
    else:
        raise Exception(" unsupported option!")
    return mask


if __name__ == "__main__":
    #m = gen_mask(256, 0.15)
    X = np.array([[1, 2, 3, 4],
                  [5, 6, 7, 8],
                  [9, 10, 11, 12]])
    Y = shuffle_2D_in_rows(X)
    print(X)
    print(Y)

