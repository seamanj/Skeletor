import argparse
from src.training import train
from src.helpers import *
from src.prediction import test, evaluate

# tj = :  command : python3 -m src train ./config.yaml
def main():
    ap = argparse.ArgumentParser("BERT Skeleton")

    ap.add_argument("--mode", choices=["train", "test", "eval"], default="train",
                    help="train a model")

    ap.add_argument("--config_path", type=str, default="../config.yaml",
                    help="path to YAML config file")

    ap.add_argument("--ckpt", type=str,
                    help="checkpoint for prediction")

    ap.add_argument("--seq_name", type=str)

    ap.add_argument("--sub_dir", type=str)

    ap.add_argument("--output_dir", type=str)

    args = ap.parse_args()

    if args.mode == "train":
        train(cfg_file=args.config_path)
    elif args.mode == "test":
        test(cfg_file=args.config_path, ckpt=args.ckpt)
    elif args.mode == "eval":
        evaluate(cfg_file=args.config_path, ckpt=args.ckpt,
             seq_name=args.seq_name, sub_dir=args.sub_dir,
             output_dir=args.output_dir)
    else:
        raise ValueError("unknown mode")


def split_dataset():
    a = load_zipped_pickle("/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton.gklz")
    b = {}
    train={}
    valid={}
    test={}
    num_frames = a['frame_data'][0].shape[0]
    num_train = int(num_frames * 0.6)
    num_valid = int(num_frames * 0.2)
    num_test = num_frames - num_train - num_valid

    train['video_name'] = a['video_name']
    train['openpose_name'] = a['openpose_name']
    train['indices'] = a['frame_data'][0][:num_train]
    train["frames"] = np.dstack((a['frame_data'][1][:num_train], a['frame_data'][2][:num_train], a['frame_data'][3][:num_train]))
    train["num_frames"] = num_train
    # a["frames"] = np.dstack((a["frames"][0], a["frames"][1], a["frames"][2]))


    valid["video_name"] = a['video_name']
    valid['openpose_name'] = a['openpose_name']
    valid['indices'] = a['frame_data'][0][num_train:num_train+num_valid]
    valid["frames"] = np.dstack((a['frame_data'][1][num_train:num_train+num_valid], a['frame_data'][2][num_train:num_train+num_valid], a['frame_data'][3][num_train:num_train+num_valid]))
    valid["num_frames"] = num_valid


    test['video_name'] = a['video_name']
    test['openpose_name'] = a['openpose_name']
    test['indices'] = a['frame_data'][0][num_train+num_valid:]
    test["frames"] = np.dstack((a['frame_data'][1][num_train+num_valid:], a['frame_data'][2][num_train+num_valid:], a['frame_data'][3][num_train+num_valid:]))
    test["num_frames"] = num_test


    data = load_zipped_pickle('/home/seamanj/Workspace/BERT_skeleton/data/dev/czech_skeleton_valid.gklz')

    save_zipped_pickle(train, "/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton_train.gklz")
    save_zipped_pickle(valid, "/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton_valid.gklz")
    save_zipped_pickle(test, "/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton_test.gklz")
    print(1)

    #c = load_zipped_pickle("/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton.gklz")
    #print(c["num_frames"])

def make_2000_dataset():
    a = load_zipped_pickle("/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton_0.gklz_bak")
    b = {}
    train={}
    valid={}
    test={}
    num_frames = 2000
    num_train = num_frames
    num_valid = 200
    num_test = 200

    train["frames"] = np.dstack((a[0][:num_train], a[1][:num_train], a[2][:num_train]))
    train["num_frames"] = num_train
    # a["frames"] = np.dstack((a["frames"][0], a["frames"][1], a["frames"][2]))

    valid["frames"] = np.dstack((a[0][:num_valid], a[1][:num_valid], a[2][:num_valid]))
    valid["num_frames"] = num_valid

    test["frames"] = np.dstack((a[0][:num_test], a[1][:num_test], a[2][:num_test]))
    test["num_frames"] = num_test


    save_zipped_pickle(train, "/home/seamanj/Workspace/BERT_skeleton/data/2000/train/czech_skeleton_train.gklz")
    save_zipped_pickle(valid, "/home/seamanj/Workspace/BERT_skeleton/data/2000/valid/czech_skeleton_valid.gklz")
    save_zipped_pickle(test, "/home/seamanj/Workspace/BERT_skeleton/data/2000/test/czech_skeleton_test.gklz")
    print(1)

    #c = load_zipped_pickle("/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton.gklz")
    #print(c["num_frames"])


def make_8_dataset():
    a = load_zipped_pickle("/home/seamanj/Workspace/BERT_skeleton/data/czech_skeleton_0.gklz_bak")
    b = {}
    train={}
    valid={}
    test={}
    num_frames = 8
    num_train = num_frames
    num_valid = 8
    num_test = 8

    train["frames"] = np.dstack((a[0][:num_train], a[1][:num_train], a[2][:num_train]))
    train["num_frames"] = num_train
    # a["frames"] = np.dstack((a["frames"][0], a["frames"][1], a["frames"][2]))

    valid["frames"] = np.dstack((a[0][:num_valid], a[1][:num_valid], a[2][:num_valid]))
    valid["num_frames"] = num_valid

    test["frames"] = np.dstack((a[0][:num_test], a[1][:num_test], a[2][:num_test]))
    test["num_frames"] = num_test


    save_zipped_pickle(train, "/home/seamanj/Workspace/BERT_skeleton/data/8/train/czech_skeleton_train.gklz")
    save_zipped_pickle(valid, "/home/seamanj/Workspace/BERT_skeleton/data/8/dev/czech_skeleton_valid.gklz")
    save_zipped_pickle(test, "/home/seamanj/Workspace/BERT_skeleton/data/8/test/czech_skeleton_test.gklz")
    print(1)

def load_data():
    a = load_zipped_pickle("/vol/research/SignPose/tj/dataset/BBC_Czech/999-ambiwlans-awyr-cymru/p02td2hz_Czech.gklz")
    print('1')


def manuplicate_data():
    a = load_zipped_pickle("/home/seamanj/Workspace/BERT_skeleton/data/V2_single_file/dev/p02td2j9_Czech.gklz")
    print('1')

if __name__ == "__main__":
    #split_dataset()
    #make_2000_dataset()
    #make_8_dataset()
    # load_data()
    # manuplicate_data()
    main()