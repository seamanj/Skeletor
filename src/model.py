# coding: utf-8
"""
Module to represents whole models
"""

import numpy as np

import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F

#from src.initialization import initialize_model
from src.embeddings import Embeddings
from src.encoders import Encoder, TransformerEncoder
from src.batch_with_mask import BatchWithMask, BatchWithMaskConf
from src.initialization import initialize_model
from src.noise import make_noise
class Model(nn.Module):
    """
    Base Model class
    """

    def __init__(self,
                 encoder: Encoder,
                 embed: Embeddings
                 ) -> None:

        super(Model, self).__init__()

        self.embed = embed
        self.encoder = encoder



    # pylint: disable=arguments-differ

    def forward(self, src: Tensor, src_mask: Tensor, src_conf: Tensor, with_c):
        """
        First encodes the source sentence.
        Then produces the target one word at a time.

        :param src: source input
        :param trg_input: target input
        :param src_mask: source mask
        :param src_lengths: length of source inputs
        :param trg_mask: target mask
        :return: decoder outputs
        """
        return self.encode(src=src, src_mask=src_mask, src_conf=src_conf, with_c=with_c)



    def encode(self, src: Tensor, src_mask: Tensor, src_conf: Tensor, with_c) :

        """
        Encodes the source sentence.

        :param src:
        :param src_length:
        :param src_mask:
        :return: encoder outputs (output, hidden_concat)
        """

        # = tj: apply the mask
        masked_src = src.clone()


        # make_noise(masked_src, src_mask, noise_weight)
        if len(src_mask.shape) == 2:
            masked_src[src_mask, :, :] = 0
        elif len(src_mask.shape) == 3:
            masked_src[src_mask, :] = 0
        else:
            raise Exception("wrong mask shape!")

        embed_skel, embed_conf = self.embed(x=masked_src, c=src_conf)

        return self.encoder(embed_src=embed_skel, embed_conf=embed_conf, with_c=with_c), masked_src




    def get_loss_for_batch(self, batch: BatchWithMaskConf, loss_function: nn.Module, with_c):

        # pylint: disable=unused-variable


        skeleton_output, masked_src = self.forward(
            src=batch.skeleton,
            src_mask=batch.mask,
            src_conf=batch.conf,
            with_c=with_c,
            )

        # compute batch loss
        batch_loss = loss_function(skeleton_output, batch.skeleton)
        # return batch loss = sum over all elements in batch that are not pad
        return batch_loss, skeleton_output, masked_src



    def __repr__(self) -> str:
        """
        String representation: a description of encoder, decoder and embeddings

        :return: string representation
        """
        return "%s(\n" \
               "\tencoder=%s,\n" \
               "\tsrc_embed=%s,\n" \
               % (self.__class__.__name__, self.encoder,
                  self.embed)


def build_model(cfg: dict = None,
                skeleton_dim: int = 150) -> Model:
    """
    Build and initialize the model according to the configuration.

    :param cfg: dictionary configuration containing model specifications
    :param src_vocab: source vocabulary
    :param trg_vocab: target vocabulary
    :return: built and initialized model
    """


    embed = Embeddings(
        **cfg["encoder"]["embeddings"], skeleton_dim=skeleton_dim)


    # build encoder
    enc_dropout = cfg["encoder"].get("dropout", 0.)
    enc_emb_dropout = cfg["encoder"]["embeddings"].get("dropout", enc_dropout)

    assert cfg["encoder"]["embeddings"]["embedding_dim"] == \
           cfg["encoder"]["hidden_size"], \
        "for transformer, emb_size must be hidden_size"

    encoder = TransformerEncoder(**cfg["encoder"],
                                     emb_size=embed.embedding_dim,
                                     emb_dropout=enc_emb_dropout,
                                     skeleton_dim=skeleton_dim)



    model = Model(encoder=encoder, embed=embed)

    # custom initialization of model parameters
    initialize_model(model, cfg)

    return model
