import torch
from torch.utils.data import Dataset, DataLoader
import os
from glob import glob
import json
from src.helpers import *


def make_data_loader(dataset: Dataset,
                     batch_size: int,
                     shuffle: bool = False) -> DataLoader:
    params = {
        'batch_size': batch_size,
        'shuffle': shuffle,
        'num_workers': 4}
    return DataLoader(dataset, **params)



def load_train_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    train_path = data_cfg.get("train_data_path")
    window_size = data_cfg.get("window_size")
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    window_translated_threshold = data_cfg.get("window_translated_threshold")
    return SkeletonDataset(train_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold)

def load_dev_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    dev_path = data_cfg.get("dev_data_path")
    window_size = data_cfg.get("window_size")
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    window_translated_threshold = data_cfg.get("window_translated_threshold")
    return SkeletonDataset(dev_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold)

def load_test_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    test_path = data_cfg.get("test_data_path")
    window_size = data_cfg.get("window_size")
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    window_translated_threshold = data_cfg.get("window_translated_threshold")
    # print(train_path)
    return SkeletonDataset(test_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold)

def load_data(data_cfg: dict) -> (Dataset, Dataset, Dataset):
    # = tj : print the train path
    train_path = data_cfg.get("train_data_path")
    dev_path = data_cfg.get("dev_data_path")
    test_path = data_cfg.get("test_data_path")
    window_size = data_cfg.get("window_size")
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    window_translated_threshold = data_cfg.get("window_translated_threshold")

    # print(train_path)
    return SkeletonDataset(train_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold), \
           SkeletonDataset(dev_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold), \
           SkeletonDataset(test_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold)


class SkeletonDataset(Dataset):
    def __init__(self, data_path, window_size, down_sample_rate, window_success_threshold, window_translated_threshold):
        self.window_size = window_size
        self.down_sample_rate = down_sample_rate
        self.window_success_threshold = window_success_threshold
        self.window_translated_threshold = window_translated_threshold

        self.frames_idx = []  # tj : list,
        self.skeleton = {}  # tj : dict
        self.confidence = {}
        self.success = {}
        self.translated = {}


        seqs = [
            {
                "root": root,
                "seq_name": y.split(".")[0],
                "input_file": os.path.join(root, y)
            }
            for root, _, files in os.walk(data_path)
            for y in files
            if y.split(".")[-1] == "gklz"
        ]

        self.skeleton_dim = 0
        for seq in seqs:
            print("start loading %s" % seq["input_file"])
            data = load_zipped_pickle(seq["input_file"])
            num_frames = len(data['success']) #

            for i in range(num_frames - window_size * down_sample_rate + 1):
                num_success_frames_in_a_window = np.sum(data['success'][i:i + window_size * down_sample_rate:down_sample_rate])
                num_translated_frames_in_a_window = np.sum(data['translated'][i:i + window_size * down_sample_rate:down_sample_rate])

                if float(num_success_frames_in_a_window) / self.window_size >= self.window_success_threshold and \
                    float(num_translated_frames_in_a_window) / self.window_size >= self.window_translated_threshold:
                    self.frames_idx.append((seq["seq_name"], i))

            self.skeleton[seq["seq_name"]] = data["skeleton"]
            self.confidence[seq['seq_name']] = data['confidence']
            self.success[seq['seq_name']] = data['success']
            self.translated[seq['seq_name']] = data['translated']

            if self.skeleton_dim == 0:
                self.skeleton_dim = np.prod(data["skeleton"].shape[1:])  # tj : the first dimension is the number of frames
            else:
                assert np.prod(data["skeleton"].shape[1:]) == self.skeleton_dim
            print("finish loading %s" % seq["input_file"])

        print('1')

    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.frames_idx)

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """
        sample = {}

        seq = self.frames_idx[index]
        seq_name = seq[0]
        frame_start_idx = seq[1] # tj : in one window, the first frame index
        assert seq_name in self.skeleton

        sample['skeleton'] = self.skeleton[seq_name][frame_start_idx:
                                             frame_start_idx + self.window_size * self.down_sample_rate:
                                             self.down_sample_rate]
        sample['confidence'] = self.confidence[seq_name][frame_start_idx:
                                             frame_start_idx + self.window_size * self.down_sample_rate:
                                             self.down_sample_rate]
        sample['success'] = self.success[seq_name][frame_start_idx:
                                             frame_start_idx + self.window_size * self.down_sample_rate:
                                             self.down_sample_rate]
        sample['translated'] = self.translated[seq_name][frame_start_idx:
                                             frame_start_idx + self.window_size * self.down_sample_rate:
                                             self.down_sample_rate]

        return sample
