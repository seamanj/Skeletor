import torch
from src.helpers import load_config, set_seed, make_model_dir, make_logger,\
    symlink_update, load_checkpoint, ConfigurationError, log_cfg, append_file
from src.data import load_data, make_data_loader, load_train_data, load_dev_data, load_test_data
from torch.utils import data
from src.model import build_model
from src.model import Model
from torch.utils.tensorboard import SummaryWriter
from src.builders import build_gradient_clipper, build_optimizer, build_scheduler
import numpy as np
import os
from torch.utils.data import Dataset
import time
from src.batch_with_mask import BatchWithMask, BatchWithMaskConf
from torch import Tensor
import queue
import shutil
from src.mask import gen_mask, gen_translated_mask, gen_joint_mask_option
from src.loss import Skeletor_Loss
from src.prediction import validate_on_data

class TrainManager:
    def __init__(self, model: Model, config: dict) -> None:
        train_config = config["training"]
        data_config = config["data"]
        self.window_size = data_config["window_size"]
        self.model_dir = make_model_dir(train_config["model_dir"],
                                        overwrite=train_config.get("overwrite", False))
        self.logger = make_logger("{}/train.log".format(self.model_dir))
        self.logging_freq = train_config.get("logging_freq", 100)
        self.valid_log_file = "{}/validations.log".format(self.model_dir)
        self.valid_report_file = "{}/validations.txt".format(self.model_dir)
        self.tb_writer = SummaryWriter(log_dir=self.model_dir + "/tensorboard/")

        self.with_confidence = train_config.get('with_confidence', False)
        self.with_sequence_ordering = data_config.get('with_sequence_ordering', False)
        #objective]
        self.loss = Skeletor_Loss()

        # model
        self.model = model
        self._log_parameters_list()

        #optimization
        self.learning_rate_min = train_config.get("learning_rate_min", 1.0e-8)
        self.clip_grad_fun = build_gradient_clipper(config=train_config)
        self.optimizer = build_optimizer(config=train_config,
                                         parameters=model.parameters())

        # validation & early stopping
        self.validation_freq = train_config.get("validation_freq", 1000)
        self.max_validation_batches = train_config.get("max_validation_batches", 0)
        self.ckpt_queue = queue.Queue(
            maxsize=train_config.get("keep_last_ckpts", 5))
        self.eval_metric = train_config.get("eval_metric", "bleu")

        self.early_stopping_metric = train_config.get("early_stopping_metric",
                                                      "loss")

        # if we schedule after BLEU/chrf, we want to maximize it, else minimize
        # early_stopping_metric decides on how to find the early stopping point:
        # ckpts are written when there's a new high/low score for this metric
        if self.early_stopping_metric in ["ppl", "loss"]:
            self.minimize_metric = True
        else:
            raise ConfigurationError(
                "Invalid setting for 'early_stopping_metric', "
                "valid options: 'loss', 'ppl'.")

        # learning rate sheduling
        self.scheduler, self.scheduler_step_at = build_scheduler(
            config=train_config,
            scheduler_mode="min" if self.minimize_metric else "max",
            optimizer=self.optimizer,
            hidden_size=config["model"]["encoder"]["hidden_size"])

        # data & batch handling
        self.shuffle = train_config.get("shuffle", True)
        self.epochs = train_config["epochs"]
        self.batch_size = train_config["batch_size"]
        self.eval_batch_size = train_config.get("eval_batch_size",
                                                self.batch_size)
        self.batch_multiplier = train_config.get("batch_multiplier", 1)

        self.mask_percentage = train_config.get("mask_percentage", 0.15)
        self.eval_mask_percentage = train_config["eval_mask_percentage"]
        self.masking_times = train_config.get("masking_times", 100)

        # CPU / GPU
        self.use_cuda = train_config["use_cuda"]
        if self.use_cuda:
            self.model.cuda()
            # self.loss.cuda()

        # initialize training statistics
        self.steps = 0
        # stop training if this flag is True by reaching learning rate minimum
        self.stop = False

        self.best_ckpt_iteration = 0
        # initial values for best scores
        self.best_ckpt_score = np.inf if self.minimize_metric else -np.inf
        # comparison function for scores
        self.is_best = lambda score: score < self.best_ckpt_score \
            if self.minimize_metric else score > self.best_ckpt_score

        # model parameters
        if "load_model" in train_config.keys():
            model_load_path = train_config["load_model"]
            self.logger.info("Loading model from %s", model_load_path)
            reset_best_ckpt = train_config.get("reset_best_ckpt", False)
            reset_scheduler = train_config.get("reset_scheduler", False)
            reset_optimizer = train_config.get("reset_optimizer", False)
            self.init_from_checkpoint(model_load_path,
                                      reset_best_ckpt=reset_best_ckpt,
                                      reset_scheduler=reset_scheduler,
                                      reset_optimizer=reset_optimizer)

    def _save_checkpoint(self) -> None:
        model_path = "{}/{}.ckpt".format(self.model_dir, self.steps)
        state = {
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            "model_state": self.model.state_dict(),
            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)
        if self.ckpt_queue.full():
            to_delete = self.ckpt_queue.get()  # delete oldest ckpt
            try:
                os.remove(to_delete)
            except FileNotFoundError:
                self.logger.warning("Wanted to delete old checkpoint %s but "
                                    "file does not exist.", to_delete)

        self.ckpt_queue.put(model_path)

        best_path = "{}/best.ckpt".format(self.model_dir)
        try:
            # create/modify symbolic link for best checkpoint
            symlink_update("{}.ckpt".format(self.steps), best_path)
        except OSError:
            # overwrite best.ckpt
            torch.save(state, best_path)

    def _save_last_checkpoint(self) -> None:
        model_path = "{}/last.ckpt".format(self.model_dir)
        state = {
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            "model_state": self.model.state_dict(),
            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)


    def init_from_checkpoint(self, path: str,
                             reset_best_ckpt: bool = False,
                             reset_scheduler: bool = False,
                             reset_optimizer: bool = False) -> None:

        model_checkpoint = load_checkpoint(path=path, use_cuda=self.use_cuda)

        # restore model and optimizer parameters
        self.model.load_state_dict(model_checkpoint["model_state"])

        if not reset_optimizer:
            self.optimizer.load_state_dict(model_checkpoint["optimizer_state"])
        else:
            self.logger.info("Reset optimizer.")

        if not reset_scheduler:
            if model_checkpoint["scheduler_state"] is not None and \
                    self.scheduler is not None:
                self.scheduler.load_state_dict(
                    model_checkpoint["scheduler_state"])
        else:
            self.logger.info("Reset scheduler.")

        # restore counts
        self.steps = model_checkpoint["steps"]

        if not reset_best_ckpt:
            self.best_ckpt_score = model_checkpoint["best_ckpt_score"]
            self.best_ckpt_iteration = model_checkpoint["best_ckpt_iteration"]
        else:
            self.logger.info("Reset tracking of the best checkpoint.")

        # move parameters to cuda
        if self.use_cuda:
            self.model.cuda()



    def _log_parameters_list(self) -> None:
        model_parameters = filter(lambda p: p.requires_grad,
                                  self.model.parameters())
        n_params = sum([np.prod(p.size()) for p in model_parameters])
        self.logger.info("Total params: %d", n_params)
        trainable_params = [n for (n, p) in self.model.named_parameters()
                            if p.requires_grad]
        self.logger.info("Trainable prameters: %s", sorted(trainable_params))
        assert trainable_params

    def train_and_validate(self, train_data: Dataset, valid_data: Dataset) -> None:
        train_loader = make_data_loader(train_data,
                                    batch_size=self.batch_size,
                                    shuffle=self.shuffle)

        for epoch_no in range(self.epochs):
            self.logger.info("EPOCH %d", epoch_no + 1)

            if self.scheduler is not None and self.scheduler_step_at == "epoch":
                self.scheduler.step(epoch=epoch_no)

            self.model.train()

            start = time.time()
            total_valid_duration = 0
            count = self.batch_multiplier - 1
            epoch_loss = 0

            for batch in train_loader:
                self.model.train()
                real_batch_size = batch['skeleton'].shape[0]
                for i in range(self.masking_times):

                    mask = gen_translated_mask(real_batch_size, self.window_size, self.mask_percentage, batch['translated'])

                    batch_with_mask_conf = BatchWithMaskConf(batch['skeleton'],\
                                                        mask, batch['confidence'],
                                                         use_cuda=self.use_cuda)

                    update = count == 0
                    batch_loss = self._train_batch(batch_with_mask_conf, update=update)
                    self.tb_writer.add_scalar("train/train_batch_loss", batch_loss,
                                          self.steps)
                    count = self.batch_multiplier if update else count
                    count -= 1
                    epoch_loss += batch_loss.detach().cpu().numpy()
                    if self.scheduler is not None and \
                            self.scheduler_step_at == "step" and update:
                        self.scheduler.step()

                    # log learning progress
                    if self.steps % self.logging_freq == 0 and update:
                        elapsed = time.time() - start - total_valid_duration
                        self.logger.info(
                            "Epoch %3d Step: %8d Batch Loss: %12.6f Lr: %12.8f",
                            epoch_no + 1, self.steps, batch_loss,
                            self.optimizer.param_groups[0]["lr"])
                        start = time.time()
                        total_valid_duration = 0

                    # validate on the entire dev set
                    if self.steps % self.validation_freq == 0 and update:
                        valid_start_time = time.time()

                        valid_loss, _ = validate_on_data(
                                batch_size=self.eval_batch_size,
                                max_validation_batches = self.max_validation_batches,
                                data=valid_data,
                                window_size=self.window_size,
                                mask_percentage=self.eval_mask_percentage,
                                model=self.model,
                                use_cuda=self.use_cuda,
                                loss_function=self.loss,
                                log_filename=self.valid_log_file,
                                with_confidence=self.with_confidence,
                            )

                        self.tb_writer.add_scalar("valid/valid_loss",
                                                  valid_loss, self.steps)

                        if self.early_stopping_metric == "loss":
                            ckpt_score = valid_loss
                        else:
                            raise ConfigurationError("Nothing else supported yet!")

                        new_best = False

                        if self.is_best(ckpt_score):
                            self.best_ckpt_score = ckpt_score
                            self.best_ckpt_iteration = self.steps
                            self.logger.info(
                                'Hooray! New best validation result [%s]!',
                                self.early_stopping_metric)
                            if self.ckpt_queue.maxsize > 0:
                                self.logger.info("Saving new checkpoint.")
                                new_best = True
                                self._save_checkpoint()
                        # else: # save the last state
                        #     self.best_ckpt_score = ckpt_score
                        #     self.best_ckpt_iteration = self.steps
                        #     # the last checkpoint is saved
                        #     self.logger.info("Saving the last checkpoint.")
                        #     self._save_last_checkpoint()

                        if self.scheduler is not None \
                                and self.scheduler_step_at == "validation":
                            self.scheduler.step(ckpt_score)

                        # append to validation report
                        self._add_report(
                            valid_loss=valid_loss,
                            new_best=new_best)
                        # = tj : in this funciton, if current_lr < self.learning_rate_min: self.stop = True

                        valid_duration = time.time() - valid_start_time
                        total_valid_duration += valid_duration
                        self.logger.info(
                            'Validation result at epoch %3d, '
                            'step %8d: loss: %8.6f '
                            'duration: %.4fs', epoch_no + 1, self.steps,
                            valid_loss,
                            valid_duration)

                        append_file(self.valid_log_file, 'Validation result at epoch %3d, '
                                                         'step %8d: loss: %8.6f, duration: %.4fs\n'
                                    %(epoch_no + 1, self.steps, valid_loss, valid_duration))

                    if self.stop:
                        break
                if self.stop:
                    break
            if self.stop:
                self.logger.info(
                    'Training ended since minimum lr %12.8f was reached.',
                    self.learning_rate_min
                )
                break
            self.logger.info('Epoch %3d: total training loss %.2f', epoch_no + 1,
                             epoch_loss)
        else:
            self.logger.info('Training ended after %3d epochs.', epoch_no+1)

        self.tb_writer.close()  # close Tensorboard writer

    def _train_batch(self, batch: BatchWithMaskConf, update: bool = True) -> (Tensor, Tensor, Tensor):

        batch_loss,_,_ = self.model.get_loss_for_batch(
            batch=batch, loss_function=self.loss, with_c=self.with_confidence)


        # division needed since loss.backward sums the gradients until updated
        norm_batch_multiply = batch_loss / self.batch_multiplier

        norm_batch_multiply.backward()

        if self.clip_grad_fun is not None:
            # clip gradients (in-place)
            self.clip_grad_fun(params=self.model.parameters())

        if update:
            self.optimizer.step()
            self.optimizer.zero_grad()

            # increment step counter
            self.steps += 1

        return batch_loss



    def _add_report(self, valid_loss: float, new_best: bool = False) -> None:
        """
        Append a one-line report to validation logging file.

        :param valid_score: validation evaluation score [eval_metric]
        :param valid_loss: validation loss (sum over whole validation set)
        :param eval_metric: evaluation metric, e.g. "bleu"
        :param new_best: whether this is a new best model
        """
        current_lr = -1
        # ignores other param groups for now
        for param_group in self.optimizer.param_groups:
            current_lr = param_group['lr']

        if current_lr < self.learning_rate_min:
            self.stop = True

        with open(self.valid_report_file, 'a') as opened_file:
            opened_file.write(
                "Steps: {}\tLoss: {:.5f}\tLR: {:.8f}\t{}\n".format(
                    self.steps, valid_loss, current_lr, "*" if new_best else ""))





def train(cfg_file: str) -> None:

    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    #training_data, dev_data, test_data = load_data(data_cfg=cfg["data"])

    training_data = load_train_data(data_cfg=cfg["data"])
    dev_data = load_dev_data(data_cfg=cfg["data"])


    model = build_model(cfg["model"], training_data.skeleton_dim)

    trainer = TrainManager(model=model, config=cfg)  # tj : all the setting pass to its member

    trainer.logger.info("train frames:{}".format(training_data.__len__()))
    trainer.logger.info("dev frames:{}".format(dev_data.__len__()))


    shutil.copy2(cfg_file, trainer.model_dir + "/config.yaml")

    # log all entries of config
    log_cfg(cfg, trainer.logger)

    #TODO : log data info

    trainer.logger.info(str(model))

    trainer.train_and_validate(train_data=training_data, valid_data=dev_data)

