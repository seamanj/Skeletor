import xmltodict
import argparse
import os
import pickle
import gzip
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math


def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)



def main(params):

    annotation_file = params.annotation_file
    input_root = params.input_root
    output_root = params.output_root
    # file_name = annotation_file.split("/")[-1]
    # sub_dir = annotation_file.replace(input_root, '')
    # sub_dir = sub_dir.replace(file_name,'')
    czech_root = params.czech_root

    print('open annotation file:{}'.format(annotation_file))
    with open(annotation_file) as fd:
        doc = xmltodict.parse(fd.read())["ANNOTATION_DOCUMENT"]

    valid = False
    for tier in doc["TIER"]:
        if tier["@TIER_ID"] == "FreeTransl" and "ANNOTATION" in tier:
            valid = True
            FreeTransl = tier
    if not valid:
        print('early returned because no FreeTransl info found')
        return


    valid = False
    header = doc["HEADER"]
    num_MEDIA_DESCRIPTOR = len(header["MEDIA_DESCRIPTOR"])

    if type(header["MEDIA_DESCRIPTOR"]) is not list:# tj : deal with only one MEDIA_DESCRIPTOR element
        media = header["MEDIA_DESCRIPTOR"]
        video_id = (
            media["@MEDIA_URL"]
                .split("/")[-1]
                .split(".")[0]
                .replace("-comp", "")
                .replace("-c", "")
        )
        if "-" not in video_id and (("(" in video_id) or ("+" not in video_id)):
            valid = True
            video_name = video_id
            offset = float(media.get("@TIME_ORIGIN", 0))
    else:
        for media in header["MEDIA_DESCRIPTOR"]:
            video_id = (
                media["@MEDIA_URL"]
                    .split("/")[-1]
                    .split(".")[0]
                    .replace("-comp", "")
                    .replace("-c", "")
            )
            if "-" not in video_id and (("(" in video_id) or ("+" not in video_id)):
                valid = True
                video_name = video_id
                offset = float(media.get("@TIME_ORIGIN", 0))


    if not valid:
        return

    time_order = doc["TIME_ORDER"]["TIME_SLOT"]
    time_slots = {
        ts["@TIME_SLOT_ID"]: float(ts["@TIME_VALUE"])
        for ts in time_order
    }


    # tj : find the czech file

    # for root, _, files in os.walk(czech_root):
    #     for name in files:
    #         print(os.path.join(root, name))
    # print(1)
    czech_files = [
        {"file_path": os.path.join(root, name)}
        for root, _, files in os.walk(czech_root)
        for name in files
        if name.startswith(video_name)
    ]

    assert len(czech_files) == 1, '{} czech_file matched {} found'.format(len(czech_files), video_name)
    czech_file = czech_files[0]['file_path']
    print('loading czech file:{}'.format(czech_file))


    file_name = czech_file.split("/")[-1]
    sub_dir = czech_file.replace(czech_root, '')
    sub_dir = sub_dir.replace(file_name,'')




    czech_data = load_gklz(czech_file)
    assert czech_data['seq_name'] == video_name

    num_frame = czech_data['success'].shape[0]

    skeletor_data = czech_data
    skeletor_data['translated'] = np.zeros(num_frame, dtype=int)

    annotations = []
    for ann in FreeTransl["ANNOTATION"]:
        start_ms = (
            time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
            + offset
        )
        stop_ms = (
            time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
            + offset
        )
        translation = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]

        start_frame = math.floor(25.0 * (start_ms / 1000.0))
        stop_frame = math.floor(25.0 * (stop_ms / 1000.0)) - 1
        
        skeletor_data['translated'][start_frame:stop_frame] = 1

        # save_data = {}
        # save_data['seq_name'] = seq_name
        # save_data['skeleton'] = np.dstack((Yx, Yy, Yz))
        # save_data['confidence'] = Xw_bak
        # save_data['success'] = Xs.astype(np.int)

        # annotations.append(
        #     {
        #         "video_name": video_name,
        #         "ann_start_ms": time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]],
        #         "ann_stop_ms": time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]],
        #         "vid_offset_ms": offset,
        #         "vid_start_ms": start_ms,
        #         "vid_stop_ms": stop_ms,
        #         "translation": translation,
        #     }
        # )

    output_dir = os.path.join(output_root, sub_dir)
    make_dir(output_dir)

    output_file = os.path.join(output_dir, "{}_Skeletor.gklz".format(video_name))
    save_gklz(obj=skeletor_data, filename=output_file)
    print('{} saved'.format(output_file))

def load_data():
    a = load_gklz("/vol/research/SignPose/tj/dataset/DCAL_Czech/Interview/Glasgow/9+10/G10i_Czech.gklz")
    print('1')

if __name__ == "__main__":

    # load_data()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--annotation_file",
        type=str,
        default="/vol/research/extol/data/BSLCP/Annotations.DCAL/Interviews/Glasgow/GW10F55WDI.eaf",
    )
    #/vol/research/extol/data/BSLCP/Annotations.DCAL/Interviews/Belfast/BF01F28WDI.eaf
    #/vol/research/extol/data/BSLCP/Annotations.DCAL/Interviews/Glasgow/GW02M33WHI.eaf

    parser.add_argument(
        "--input_root",
        type=str,
        default="/vol/research/extol/data/BSLCP/Annotations.DCAL/",
        help="",
    )
    parser.add_argument(
        "--czech_root",
        type=str,
        default="/vol/research/SignPose/tj/dataset/DCAL_Czech/",
        help="",
    )
    parser.add_argument(
        "--output_root",
        type=str,
        default="/vol/research/SignPose/tj/dataset/DCAL_Skeletor/",
    )
    params, _ = parser.parse_known_args()
    main(params)
