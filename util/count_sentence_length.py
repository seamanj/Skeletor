import xmltodict
import argparse
import os
import pickle
import gzip
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math

def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)



def main(params):



    sentence_length_map = {}

    input_root = params.input_root
    output_root = params.output_root
    make_dir(output_root)
    xy_tmp_file = os.path.join(output_root, 'xy.gklz')
    if os.path.exists(xy_tmp_file):
        xy = load_gklz(xy_tmp_file)
        x = xy['x']
        y = xy['y']
    else:
        for root, dirs, files in os.walk(input_root):
            gen = (file for file in files if '.gklz' in file) # tj : generator expressions  https://stackoverflow.com/questions/6981717/pythonic-way-to-combine-for-loop-and-if-statement
            for file in gen:
                skeletor_file = os.path.join(root, file)
                print('Loading {}'.format(skeletor_file))
                skeletor_data = load_gklz(skeletor_file)
                translated_data = skeletor_data['translated']
                num_frame = len(translated_data)
                length = 0
                for i in np.arange(num_frame):
                    if translated_data[i] == 0 and length != 0:
                        if length in sentence_length_map:
                            sentence_length_map[length] += 1
                        else:
                            sentence_length_map[length] = 1
                        length = 0
                    elif translated_data[i] == 1:
                        length += 1

        sentence_length_map = dict(sorted(sentence_length_map.items())) # tj : sort the dict, https://stackoverflow.com/questions/9001509/how-can-i-sort-a-dictionary-by-key

        # = tj : use pandas to draw bar chart
        # df = pd.DataFrame({'number':list(sentence_length_map.values())}, index=list(sentence_length_map.keys()))
        # # df = df.astype(int)
        # ax = df.plot.bar(rot=0)

        # x = list(sentence_length_map.keys())
        # y = list(sentence_length_map.values())

        # = tj : https://stackoverflow.com/questions/23668509/dictionary-keys-and-values-to-separate-numpy-arrays
        x = np.fromiter(sentence_length_map.keys(), dtype=int) # tj : get numpy array
        y = np.fromiter(sentence_length_map.values(), dtype=int)

        save_data ={}
        save_data['x'] = x
        save_data['y'] = y
        save_gklz(save_data, xy_tmp_file)


    weighed_average_length = sum(x*y) / sum(y)
    print('the weiged average sentence length is: {}'.format(weighed_average_length))


    xx = np.arange(max(x) - min(x) + 1)

    yy = np.zeros(max(x) - min(x) + 1)
    yy[x - min(x)] = y


    plt.figure(figsize=(25.60, 14.40))
    plt.bar(xx + min(x), yy, align='center', color='k') # tj : draw bar chart, https://pythonspot.com/matplotlib-bar-chart/


    # plt.plot(xx + min(x), yy, 'k-') # tj : draw curve
    plt.xticks(np.arange(0, (max(x) / 10 + 1) * 10, 10)) # tj : change the tick density:
    plt.ylim(ymin=0) # tj : set the bottom of y from 0
    #plt.show()

    plt.tight_layout()

    plt.savefig(os.path.join(output_root, 'chart.png'), dpi=100)

    file = open(os.path.join(output_root, 'statistics.txt'), "w")
    file.write('minimal length: {}\n'.format(min(x)))
    file.write('maximum length: {}\n'.format(max(x)))
    file.write('weighted average length: {}'.format(weighed_average_length))

    print(1)






    #

def load_data():
    a = load_gklz("/vol/research/SignPose/tj/dataset/DCAL_Skeletor/Interview/Newcastle/5+6/N6i_Skeletor.gklz")
    print('1')



if __name__ == "__main__":

    # load_data()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input_root",
        type=str,
        default="/vol/research/SignPose/tj/dataset/DCAL_Skeletor/Interview",
        help="",
    )

    parser.add_argument(
        "--output_root",
        type=str,
        default="/vol/research/SignPose/tj/Workspace/Skeletor_New/Skeletor/Sentence_length",
        help="",
    )

    params, _ = parser.parse_known_args()
    main(params)
